package com.blockbuster.project;

import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import com.blockbuster.project.entities.Cliente;
import com.blockbuster.project.repositories.ClienteRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
class ProjectApplicationTests {

	@Autowired
	private ClienteRepository clienteRepository;
	
	@Autowired
	private BCryptPasswordEncoder encoder;
	
	@Test
	void crearClienteTest() {
		Cliente cliente = new Cliente();
		cliente.setName("Admin");
		cliente.setLastname("Administrador");
		cliente.setUsername("admin");
		cliente.setPassword(encoder.encode("admin"));
		cliente.setEmail("ip@gmail.com");
		cliente.setDni("11223321t");
		Cliente retorno = clienteRepository.save(cliente);
		
		assertTrue(retorno.getPassword().equalsIgnoreCase(cliente.getPassword()));
	}

}
