package com.blockbuster.project.service;

import java.util.List;

import com.blockbuster.project.dto.CompaniaDto;

public interface CompaniaService {
	
	public List<CompaniaDto> get();
	
	public CompaniaDto get(Long id);
	
	public CompaniaDto create(CompaniaDto compania);
	
	public CompaniaDto update(Long id, CompaniaDto companiaDto);
	
	public void delete(Long id);
	
}
