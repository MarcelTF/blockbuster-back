package com.blockbuster.project.service;

import java.util.List;

import com.blockbuster.project.dto.ClienteDto;

public interface ClienteService {
	
	public List<ClienteDto> get();
	
	public ClienteDto get(Long id);
	
	public ClienteDto create(ClienteDto cliente);
	
	public ClienteDto update(Long id, ClienteDto clienteDto);
	
	public void delete(Long id);

}
