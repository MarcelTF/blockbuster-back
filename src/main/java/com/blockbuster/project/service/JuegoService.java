package com.blockbuster.project.service;

import java.util.List;

import com.blockbuster.project.dto.JuegoDto;

public interface JuegoService {

    public List<JuegoDto> get();

    public JuegoDto create(JuegoDto juego);

    public JuegoDto get(Long id);

    public JuegoDto update(Long id, JuegoDto juego);

    public void delete(Long id);
	
}
