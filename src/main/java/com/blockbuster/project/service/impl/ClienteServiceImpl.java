package com.blockbuster.project.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.blockbuster.project.controller.ClienteController;
import com.blockbuster.project.converter.ClienteConverter;
import com.blockbuster.project.converter.RolConverter;
import com.blockbuster.project.converter.StockConverter;
import com.blockbuster.project.dto.ClienteDto;
import com.blockbuster.project.entities.Cliente;
import com.blockbuster.project.exceptions.ClienteBadRequestException;
import com.blockbuster.project.repositories.ClienteRepository;
import com.blockbuster.project.repositories.StockRepository;
import com.blockbuster.project.service.ClienteService;

@Service
public class ClienteServiceImpl implements ClienteService{
	
	@Autowired
	ClienteRepository clienteRepository;
	
	@Autowired
	StockRepository stockRepository;
	
	@Autowired
	ClienteConverter clienteConverter;
	
	@Autowired
	StockConverter stockConverter;
	
	@Autowired
	RolConverter rolConverter;
	
	@Autowired
	private BCryptPasswordEncoder encoder;

	@Override
	public List<ClienteDto> get() {
		return clienteRepository.findAll().stream().map(cliente -> clienteConverter.clienteToDto(cliente)).collect(Collectors.toList());
	}
	
	@Override
	public ClienteDto get(Long id) {
		return clienteConverter.clienteToDto(clienteRepository.findById(id).orElse(null));
	}
	
	@Override
	public ClienteDto create(ClienteDto clienteDto) {
		Cliente newCliente = clienteConverter.clienteToEnt(clienteDto);
		newCliente.setPassword(encoder.encode(newCliente.getPassword()));
		return clienteConverter.clienteToDto(clienteRepository.save(newCliente));
		
	}
	
	@Override
	public ClienteDto update(Long id, ClienteDto clienteDto) {
		Cliente newCliente = clienteConverter.clienteToEnt(clienteDto);
		newCliente.setIdCliente(id);
		newCliente.setPassword(encoder.encode(newCliente.getPassword()));
		return clienteConverter.clienteToDto(clienteRepository.save(newCliente));	

	}
	
	@Override
	public void delete(Long id) {
		clienteRepository.deleteById(id);
	}

}
