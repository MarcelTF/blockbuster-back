package com.blockbuster.project.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blockbuster.project.converter.JuegoConverter;
import com.blockbuster.project.dto.JuegoDto;
import com.blockbuster.project.entities.Juego;
import com.blockbuster.project.repositories.JuegoRepository;
import com.blockbuster.project.service.JuegoService;

@Service
public class JuegoServiceImpl implements JuegoService {

	@Autowired
	JuegoRepository juegoRepository;
	
	@Autowired
	JuegoConverter juegoConverter;
	
    @Override
    public List<JuegoDto> get() {
        return juegoRepository.findAll().stream().map(juego -> juegoConverter.juegoToDto(juego)).collect(Collectors.toList());
    }

    @Override
    public JuegoDto create(JuegoDto juegoDto) {
        Juego juego = juegoConverter.juegoToEnt(juegoDto);
        return juegoConverter.juegoToDto(juegoRepository.save(juego));
    }

    @Override
    public JuegoDto get(Long id) {
        return juegoConverter.juegoToDto(juegoRepository.findById(id).get());
    }

    @Override
    public JuegoDto update(Long id, JuegoDto juegoDto) {
        Juego juego = juegoConverter.juegoToEnt(juegoDto);
        juego.setIdJuego(id);
        return juegoConverter.juegoToDto(juegoRepository.save(juego));
    }

    @Override
    public void delete(Long id) {
        juegoRepository.deleteById(id);
    }
}
