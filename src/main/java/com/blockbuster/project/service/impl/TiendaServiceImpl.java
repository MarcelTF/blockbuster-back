package com.blockbuster.project.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blockbuster.project.converter.TiendaConverter;
import com.blockbuster.project.dto.TiendaDto;
import com.blockbuster.project.entities.Tienda;
import com.blockbuster.project.repositories.TiendaRepository;
import com.blockbuster.project.service.TiendaService;

@Service
public class TiendaServiceImpl implements TiendaService {
	
	@Autowired
	TiendaRepository tiendaRepository;
	
	@Autowired
	TiendaConverter tiendaConverter;

    @Override
    public List<TiendaDto> get() {
        return tiendaRepository.findAll().stream().map(tienda -> tiendaConverter.tiendaToDto(tienda)).collect(Collectors.toList());
    }

    @Override
    public TiendaDto create(TiendaDto tiendaDto) {
        Tienda tienda = tiendaConverter.tiendaToEnt(tiendaDto);
        return tiendaConverter.tiendaToDto(tiendaRepository.save(tienda));
    }

    @Override
    public TiendaDto get(Long id) {
        return tiendaConverter.tiendaToDto(tiendaRepository.findById(id).get());
    }

    @Override
    public TiendaDto update(Long id, TiendaDto tiendaDto) {
        Tienda tienda = tiendaConverter.tiendaToEnt(tiendaDto);
        tienda.setIdTienda(id);
        return tiendaConverter.tiendaToDto(tiendaRepository.save(tienda));
    }

    @Override
    public void delete(Long id) {
        tiendaRepository.deleteById(id);
    }
}
