package com.blockbuster.project.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blockbuster.project.converter.CompaniaConverter;
import com.blockbuster.project.dto.CompaniaDto;
import com.blockbuster.project.entities.Compania;
import com.blockbuster.project.repositories.CompaniaRepository;
import com.blockbuster.project.service.CompaniaService;

@Service
public class CompaniaServiceImpl implements CompaniaService {

	@Autowired
	CompaniaRepository companiaRepository;
	
	@Autowired
	CompaniaConverter companiaConverter;

    @Override
    public List<CompaniaDto> get() {
        return companiaRepository.findAll().stream().map(compania -> companiaConverter.companiaToDto(compania)).collect(Collectors.toList());
    }

    @Override
    public CompaniaDto create(CompaniaDto companiaDto) {
        Compania compania = companiaConverter.companiaToEnt(companiaDto);
        return companiaConverter.companiaToDto(companiaRepository.save(compania));
    }

    @Override
    public CompaniaDto get(Long id) {
        return companiaConverter.companiaToDto(companiaRepository.findById(id).get());
    }

    @Override
    public CompaniaDto update(Long id, CompaniaDto companiaDto) {
        Compania compania = companiaConverter.companiaToEnt(companiaDto);
        compania.setIdCompania(id);
        return companiaConverter.companiaToDto(companiaRepository.save(compania));
    }

    @Override
    public void delete(Long id) {
        companiaRepository.deleteById(id);
    }
}
