package com.blockbuster.project.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blockbuster.project.converter.StockConverter;
import com.blockbuster.project.dto.StockDto;
import com.blockbuster.project.entities.Stock;
import com.blockbuster.project.repositories.StockRepository;
import com.blockbuster.project.service.StockService;

@Service
public class StockServiceImpl implements StockService {
	
	@Autowired
	StockRepository stockRepository;
	
	@Autowired
	StockConverter stockConverter;
	
    @Override
    public List<StockDto> get() {
        return stockRepository.findAll().stream().map(stock -> stockConverter.stockToDto(stock)).collect(Collectors.toList());
    }

    @Override
    public StockDto create(StockDto stockDto) {
        Stock stock = stockConverter.stockToEnt(stockDto);
        return stockConverter.stockToDto(stockRepository.save(stock));
    }

    @Override
    public StockDto get(Long id) {
        return stockConverter.stockToDto(stockRepository.findById(id).get());
    }

    @Override
    public StockDto update(Long id, StockDto stockDto) {
        Stock stock = stockConverter.stockToEnt(stockDto);
        stock.setIdStock(id);
        return stockConverter.stockToDto(stockRepository.save(stock));
    }

    @Override
    public void delete(Long id) {
        stockRepository.deleteById(id);
    }

}
