package com.blockbuster.project.service;

import java.util.List;

import com.blockbuster.project.dto.TiendaDto;

public interface TiendaService {
	
    public List<TiendaDto> get();

    public TiendaDto create(TiendaDto tienda);

    public TiendaDto get(Long id);

    public TiendaDto update(Long id, TiendaDto tienda);

    public void delete(Long id);

}
