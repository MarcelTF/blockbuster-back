package com.blockbuster.project.service;

import java.util.List;

import com.blockbuster.project.dto.StockDto;

public interface StockService {

    public List<StockDto> get();

    public StockDto create(StockDto stockDto);

    public StockDto get(Long id);

    public StockDto update(Long id, StockDto stockDto);

    public void delete(Long id);
}
