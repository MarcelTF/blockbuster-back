package com.blockbuster.project.dto;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
public class ClienteDto {
	
	private Long idCliente;

	@NotBlank(message = "El nombre es un campo obligatorio")
	private String name;
	
	@NotBlank(message = "El apellido es un campo obligatorio")
	private String lastname;
	
	@NotBlank(message = "El usuario es un campo obligatorio")
	private String username;
	
	@NotBlank(message = "La contraseña es un campo obligatorio")
	private String password;
	
	@NotBlank(message = "El DNI es un campo obligatorio")
	@Pattern(regexp = "^[0-9]{8}[A-Za-z]{1}$", message="El formato del DNI no es valido")
	private String dni;
	
	@NotBlank(message = "El correo es un campo obligatorio")
	@Email(message = "Introduce una cuenta de correo válida")
	private String email;
	
	@NotNull
	@JsonFormat(pattern = "dd/MM/yyyy", locale = "ES")
	private LocalDate birthday;
	
	private List<StockDto> stocks = new ArrayList<StockDto>();
	
	private List<RolDto> rols = new ArrayList<RolDto>();

}
