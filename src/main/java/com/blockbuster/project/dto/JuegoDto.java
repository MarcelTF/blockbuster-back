package com.blockbuster.project.dto;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.blockbuster.project.enums.CategoriasEnum;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
public class JuegoDto {
	
	private Long idJuego;

	@NotBlank(message = "El titulo es un campo obligatorio")
	private String title;
	
	@NotNull
	@JsonFormat(pattern = "dd/MM/yyyy", locale = "ES")
	private LocalDate launchDate;
	
	@NotNull
	private Integer price, pegi;
	
	private CategoriasEnum category;
	
	private List<CompaniaDto> companies = new ArrayList<CompaniaDto>();

}
