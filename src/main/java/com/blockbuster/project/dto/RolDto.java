package com.blockbuster.project.dto;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;

import com.blockbuster.project.enums.RolesEnum;

import lombok.Data;

@Data
public class RolDto {
	
	private Long idRol;
	
	@NotNull(message = "Se debe asignar un rol")
	private RolesEnum rol;
	
	private List<ClienteDto> clientes = new ArrayList<ClienteDto>();

}
