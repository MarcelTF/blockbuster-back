package com.blockbuster.project.dto;

import com.blockbuster.project.enums.EstadoEnum;

import lombok.Data;

@Data
public class StockDto {
	
	private Long idStock;

	private String referencia;
	
	private EstadoEnum estado;
	
	private ClienteDto cliente;
	
	private TiendaDto tienda;
	
	private JuegoDto juego;

}
