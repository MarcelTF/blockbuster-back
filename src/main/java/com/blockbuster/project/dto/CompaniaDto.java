package com.blockbuster.project.dto;

import java.util.List;

import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
public class CompaniaDto {
	
	private Long idCompania;
	
	private String cif;

	private String name;
	
	private List<JuegoDto> juegos;

}
