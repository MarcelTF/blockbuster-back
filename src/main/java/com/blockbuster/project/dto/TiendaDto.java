package com.blockbuster.project.dto;

import java.util.List;

import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
public class TiendaDto {
	
	private Long idTienda;
	
	@NotBlank
	private String name, address;
	
	private List<StockDto> stock;
	
}
