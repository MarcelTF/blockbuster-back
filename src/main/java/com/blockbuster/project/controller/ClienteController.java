package com.blockbuster.project.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.blockbuster.project.dto.ClienteDto;
import com.blockbuster.project.service.ClienteService;

@RestController
@RequestMapping("api/clientes")
public class ClienteController {
	
	@Autowired
	private ClienteService clienteService;

	@GetMapping
	public List<ClienteDto> get() {
		return clienteService.get();
	}
	
	@GetMapping("/{id}")
	public ClienteDto get(@PathVariable Long id) {
		return clienteService.get(id);
	}
	
	@PostMapping
	public ClienteDto create(@Valid @RequestBody ClienteDto clienteDto) {
		return clienteService.create(clienteDto);
	}
	
	@PutMapping("/{id}")
	public ClienteDto update(@PathVariable Long id, @Valid @RequestBody ClienteDto clienteDto) {
		clienteService.update(id, clienteDto);
		return null;
	}
	
	@DeleteMapping("/{id}")
	public void delete(@PathVariable Long id) {
		clienteService.delete(id);
	}	
	
}
