package com.blockbuster.project.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.blockbuster.project.dto.TiendaDto;
import com.blockbuster.project.service.TiendaService;

@RestController
@RequestMapping("api/tiendas")
public class TiendaController {
	
	@Autowired
	private TiendaService tiendaService;

	@GetMapping
	public List<TiendaDto> get() {
		return tiendaService.get();
	}
	
	@GetMapping("/{id}")
	public TiendaDto get(@PathVariable Long id) {
		return tiendaService.get(id);
	}
	
	@PostMapping
	public TiendaDto create(@Valid @RequestBody TiendaDto tiendaDto) {
		return tiendaService.create(tiendaDto);
	}
	
	@PutMapping("/{id}")
	public TiendaDto update(@PathVariable Long id, @Valid @RequestBody TiendaDto tiendaDto) {
		tiendaService.update(id, tiendaDto);
		return null;
	}
	
	@DeleteMapping("/{id}")
	public void delete(@PathVariable Long id) {
		tiendaService.delete(id);
	}	

}
