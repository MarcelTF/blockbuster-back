package com.blockbuster.project.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.blockbuster.project.dto.CompaniaDto;
import com.blockbuster.project.service.CompaniaService;

@RestController
@RequestMapping("api/companias")
public class CompaniaController {
	
	@Autowired
	private CompaniaService companiaService;
	
	@GetMapping
	public List<CompaniaDto> get() {
		return companiaService.get();
	}
	
	@GetMapping("/{id}")
	public CompaniaDto get(@PathVariable Long id) {
		return companiaService.get(id);
	}
	
	@PostMapping
	public CompaniaDto create(@Valid @RequestBody CompaniaDto companiaDto) {
		return companiaService.create(companiaDto);
	}
	
	@PutMapping("/{id}")
	public CompaniaDto update(@PathVariable Long id, @Valid @RequestBody CompaniaDto companiaDto) {
		companiaService.update(id, companiaDto);
		return null;
	}
	
	@DeleteMapping("/{id}")
	public void delete(@PathVariable Long id) {
		companiaService.delete(id);
	}	

}
