package com.blockbuster.project.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.blockbuster.project.dto.StockDto;
import com.blockbuster.project.service.StockService;

@RestController
@RequestMapping("/api/stocks")
public class StockController {

	@Autowired
	StockService stockService;
	
    @GetMapping
    public List<StockDto> get() {
        return stockService.get();
    }

    @GetMapping("/{id}")
    public StockDto get(@PathVariable Long id) {
        return stockService.get(id);
    }

    @PostMapping
    public StockDto create(@Valid @RequestBody StockDto stockDto) {
        return stockService.create(stockDto);
    }

    @PutMapping("/{id}")
    public StockDto update(@PathVariable Long id, @Valid @RequestBody StockDto stockDto) {
        return stockService.update(id, stockDto);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        stockService.delete(id);
    }
	
}