package com.blockbuster.project.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.blockbuster.project.dto.JuegoDto;
import com.blockbuster.project.service.JuegoService;

@RestController
@RequestMapping("api/juegos")
public class JuegoController {

    @Autowired
    private JuegoService juegoService;

    @GetMapping
    public List<JuegoDto> getJ() {
        return juegoService.get();
    }

    @GetMapping("/{id}")
    public JuegoDto get(@PathVariable Long id) {
        return juegoService.get(id);
    }

    @PostMapping
    public JuegoDto create(@Valid @RequestBody JuegoDto juego) {
        return juegoService.create(juego);
    }

    @PutMapping("/{id}")
    public JuegoDto update(@PathVariable Long id, @Valid @RequestBody JuegoDto juego) {
        return juegoService.update(id, juego);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        juegoService.delete(id);
    }

}

