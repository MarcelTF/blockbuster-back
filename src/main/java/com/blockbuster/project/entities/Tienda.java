package com.blockbuster.project.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "TIENDA")
@Data
public class Tienda {

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "idTienda")
	private Long idTienda;
	
	@Column(name = "NOMBRE")
	private String name;
	
	@Column(name = "DIRECCION")
	private String address;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "tienda")
	private List<Stock> stock = new ArrayList<Stock>();
}
