package com.blockbuster.project.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.blockbuster.project.enums.RolesEnum;

import lombok.Data;


@Entity
@Table(name = "ROL")
@Data
public class Rol {

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "idRol")
	private Long idRol;

	@Column(name = "ROL")
	private RolesEnum rol;
	
	@ManyToMany
	private List<Cliente> clientes;
}
