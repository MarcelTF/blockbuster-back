package com.blockbuster.project.entities;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Entity
@Table(name = "CLIENTE")
@Data
public class Cliente {

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "idCliente")
	private Long idCliente;
	
	@Column(name = "NOMBRE")
	private String name;
	
	@Column(name = "APELLIDO")
	private String lastname;
	
	@Column(name = "USUARIO")
	private String username;
	
	@Column(name = "PASSWORD")
	private String password;
	
	@Column(name = "DNI")
	private String dni;
	
	@Column(name = "EMAIL")
	private String email;
	
	@JsonFormat(pattern = "dd/MM/yyyy")
	@Column(name = "F.NACIMIENTO")
	private LocalDate birthday;
	
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "cliente")
	private List<Stock> stocks = new ArrayList<Stock>();
	
	@ManyToMany
	private List<Rol> roles = new ArrayList<Rol>();
	
}
