package com.blockbuster.project.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "COMPANIA")
@Data
public class Compania {

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "idCompania")
	private Long idCompania;
	
	@Column(name = "CIF")
	private String cif;
	
	@Column(name = "NOMBRE")
	private String name;
	
	@ManyToMany(mappedBy = "companies", cascade = CascadeType.ALL)
	private List<Juego> juegos = new ArrayList<Juego>();
}
