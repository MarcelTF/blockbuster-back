package com.blockbuster.project.entities;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.blockbuster.project.enums.CategoriasEnum;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Entity
@Table(name = "JUEGO")
@Data
public class Juego {

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "idJuego")
	private Long idJuego;
	
	@Column(name = "TITULO")
	private String title;
	
	@Column(name = "LANZAMIENTO")
	@JsonFormat(pattern = "dd/MM/yyyy")
	private LocalDate launchDate;
	
	@Column(name = "CATEGORIA")
	private CategoriasEnum category;
	
	@Column(name = "PRECIO")
	private Integer price;
	
	@Column(name = "PEGI")
	private Integer pegi;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "juego")
	private List<Stock> stocks = new ArrayList<Stock>();
	
	@ManyToMany
	private List<Compania> companies = new ArrayList<Compania>();
}