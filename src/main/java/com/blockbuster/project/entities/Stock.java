package com.blockbuster.project.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.blockbuster.project.enums.EstadoEnum;

import lombok.Data;

@Entity
@Table(name = "STOCK")
@Data
public class Stock {

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "idStock")
	private Long idStock;
	
	@Column(name = "REFERENCIA")
	private String referencia;
	
	@Column(name = "ESTADO")
	private EstadoEnum estado;
	
	@ManyToOne
	private Cliente cliente;
	
	@ManyToOne
	private Tienda tienda;
	
	@ManyToOne
	private Juego juego;
}