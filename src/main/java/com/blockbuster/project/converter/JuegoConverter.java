package com.blockbuster.project.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blockbuster.project.dto.CompaniaDto;
import com.blockbuster.project.dto.JuegoDto;
import com.blockbuster.project.entities.Compania;
import com.blockbuster.project.entities.Juego;

@Service
public class JuegoConverter {
	
	@Autowired
	CompaniaConverter companiaConverter;

	public JuegoDto juegoToDto(Juego juego) {
		
		JuegoDto j = new JuegoDto();
		
		j.setIdJuego(juego.getIdJuego());
		j.setTitle(juego.getTitle());
		j.setCategory(juego.getCategory());
		j.setLaunchDate(juego.getLaunchDate());
		j.setPrice(juego.getPrice());
		j.setPegi(juego.getPegi());
		
		List<CompaniaDto> allCompanias = new ArrayList<>();
		for (Compania compania:juego.getCompanies()) {
			
			CompaniaDto c = companiaConverter.companiaToDto(compania);
			allCompanias.add(c);
		}
		j.setCompanies(allCompanias);
		
		return j;
	}
	
	public Juego juegoToEnt(JuegoDto juegoDto) {
		
		Juego j = new Juego();
		
		j.setIdJuego(juegoDto.getIdJuego());
		j.setTitle(juegoDto.getTitle());
		j.setCategory(juegoDto.getCategory());
		j.setLaunchDate(juegoDto.getLaunchDate());
		j.setPrice(juegoDto.getPrice());
		j.setPegi(juegoDto.getPegi());
		
		List<Compania> allCompanias = new ArrayList<>();
		for (CompaniaDto companiaDto:juegoDto.getCompanies()) {
			
			Compania c = companiaConverter.companiaToEnt(companiaDto);
			allCompanias.add(c);
		}
		j.setCompanies(allCompanias);
		
		return j;
	}
}
