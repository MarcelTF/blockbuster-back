package com.blockbuster.project.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blockbuster.project.dto.StockDto;
import com.blockbuster.project.dto.TiendaDto;
import com.blockbuster.project.entities.Stock;
import com.blockbuster.project.entities.Tienda;

@Service
public class TiendaConverter {
	
	@Autowired
	StockConverter stockConverter;

	public TiendaDto tiendaToDto(Tienda tienda) {
		
		TiendaDto t = new TiendaDto();
		
		t.setIdTienda(tienda.getIdTienda());
		t.setName(tienda.getName());
		t.setAddress(tienda.getAddress());
		
		List<StockDto> allStocks = new ArrayList<>();
		for (Stock stock:tienda.getStock()) {
			
			StockDto s = stockConverter.stockToDto(stock);
			allStocks.add(s);
		}
		t.setStock(allStocks);
		
		return t;
	}
	
	public Tienda tiendaToEnt(TiendaDto tiendaDto) {
		
		Tienda t = new Tienda();
		
		t.setIdTienda(tiendaDto.getIdTienda());
		t.setName(tiendaDto.getName());
		t.setAddress(tiendaDto.getAddress());
		
		List<Stock> allStocks = new ArrayList<>();
		for (StockDto stockDto:tiendaDto.getStock()) {
			
			Stock s = stockConverter.stockToEnt(stockDto);	
			allStocks.add(s);
		}
		t.setStock(allStocks);
		
		return t;
	}
}
