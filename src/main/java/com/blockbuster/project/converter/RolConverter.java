package com.blockbuster.project.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blockbuster.project.dto.ClienteDto;
import com.blockbuster.project.dto.RolDto;
import com.blockbuster.project.entities.Cliente;
import com.blockbuster.project.entities.Rol;

@Service
public class RolConverter {
	
	@Autowired
	ClienteConverter clienteConverter;

	public RolDto rolToDto(Rol rol) {
		
		RolDto r = new RolDto();
		
		r.setRol(rol.getRol());
		
		List<ClienteDto> allClientes = new ArrayList<>();
		for (Cliente cliente:rol.getClientes()) {
			
			ClienteDto s = clienteConverter.clienteToDto(cliente);
			allClientes.add(s);
		}
		r.setClientes(allClientes);
		
		return r;
	}
	
	public Rol rolToEnt(RolDto rolDto) {
		
		Rol r = new Rol();
		
		r.setRol(rolDto.getRol());
		
		List<Cliente> allClientes = new ArrayList<>();
		for (ClienteDto clienteDto:rolDto.getClientes()) {
			
			Cliente s = clienteConverter.clienteToEnt(clienteDto);
			allClientes.add(s);
		}
		r.setClientes(allClientes);
		
		return r;
	}
}
