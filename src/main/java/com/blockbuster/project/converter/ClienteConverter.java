package com.blockbuster.project.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blockbuster.project.dto.ClienteDto;
import com.blockbuster.project.dto.RolDto;
import com.blockbuster.project.dto.StockDto;
import com.blockbuster.project.entities.Cliente;
import com.blockbuster.project.entities.Rol;
import com.blockbuster.project.entities.Stock;
import com.blockbuster.project.repositories.ClienteRepository;

@Service
public class ClienteConverter {
	
	@Autowired
	ClienteRepository clienteRepository;
	
	@Autowired
	StockConverter stockConverter;
	
	@Autowired
	RolConverter rolConverter;
	
	public ClienteDto clienteToDto(Cliente cliente) {
		
		ClienteDto c = new ClienteDto();
		
		c.setIdCliente(cliente.getIdCliente());
		c.setName(cliente.getName());
		c.setLastname(cliente.getLastname());
		c.setBirthday(cliente.getBirthday());
		c.setEmail(cliente.getEmail());
		c.setDni(cliente.getDni());
		c.setUsername(cliente.getUsername());
		c.setPassword(cliente.getPassword());
		
		List<StockDto> allStocks = new ArrayList<>();
		for (Stock stock:cliente.getStocks()) {
			
			StockDto s = stockConverter.stockToDto(stock);
			allStocks.add(s);
		}
		c.setStocks(allStocks);
		
		List<RolDto> allRoles = new ArrayList<>();
		for (Rol rol:cliente.getRoles()) {
			
			RolDto r = rolConverter.rolToDto(rol);
			allRoles.add(r);
		}
		c.setRols(allRoles);
		
		return c;
	}
	
	public Cliente clienteToEnt(ClienteDto clienteDto) {
		
		Cliente c = new Cliente();
		
		c.setIdCliente(clienteDto.getIdCliente());
		c.setName(clienteDto.getName());
		c.setLastname(clienteDto.getLastname());
		c.setBirthday(clienteDto.getBirthday());
		c.setEmail(clienteDto.getEmail());
		c.setDni(clienteDto.getDni());
		c.setUsername(clienteDto.getUsername());
		c.setPassword(clienteDto.getPassword());
		
		List<Stock> allStocks = new ArrayList<>();
		for (StockDto stockDto:clienteDto.getStocks()) {
			
			Stock s = stockConverter.stockToEnt(stockDto);	
			allStocks.add(s);
		}
		c.setStocks(allStocks);
		
		List<Rol> allRoles = new ArrayList<>();
		for (RolDto rolDto:clienteDto.getRols()) {
			
			Rol r = rolConverter.rolToEnt(rolDto);
			allRoles.add(r);
		}
		c.setRoles(allRoles);
		
		return c;
	}

}
