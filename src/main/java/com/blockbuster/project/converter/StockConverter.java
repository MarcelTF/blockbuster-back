package com.blockbuster.project.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blockbuster.project.dto.StockDto;
import com.blockbuster.project.entities.Stock;
import com.blockbuster.project.repositories.ClienteRepository;
import com.blockbuster.project.repositories.StockRepository;

@Service
public class StockConverter {
	
	@Autowired
	ClienteRepository clienteRepository;
	
	@Autowired
	StockRepository stockRepository;
	
	@Autowired
	JuegoConverter juegoConverter;
	
	@Autowired
	ClienteConverter clienteConverter;
	
	@Autowired
	TiendaConverter tiendaConverter;
	
	public StockDto stockToDto(Stock stock) {
		
		StockDto s = new StockDto();
		
		s.setIdStock(stock.getIdStock());
		s.setEstado(stock.getEstado());
		s.setReferencia(stock.getReferencia());
		s.setJuego(juegoConverter.juegoToDto(stock.getJuego()));
		s.setCliente(clienteConverter.clienteToDto(stock.getCliente()));
		s.setTienda(tiendaConverter.tiendaToDto(stock.getTienda()));
		
		return s;
	}
	
	public Stock stockToEnt(StockDto stockDto) {
		
		Stock s = new Stock();
		
		s.setIdStock(stockDto.getIdStock());
		s.setEstado(stockDto.getEstado());
		s.setReferencia(stockDto.getReferencia());	
		s.setJuego(juegoConverter.juegoToEnt(stockDto.getJuego()));
		s.setCliente(clienteConverter.clienteToEnt(stockDto.getCliente()));
		s.setTienda(tiendaConverter.tiendaToEnt(stockDto.getTienda()));
		
		return s;
	}

}
