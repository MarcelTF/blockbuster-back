package com.blockbuster.project.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blockbuster.project.dto.CompaniaDto;
import com.blockbuster.project.dto.JuegoDto;
import com.blockbuster.project.entities.Compania;
import com.blockbuster.project.entities.Juego;

@Service
public class CompaniaConverter {
	
	@Autowired
	JuegoConverter juegoConverter;

	public CompaniaDto companiaToDto(Compania compania) {
		
		CompaniaDto c = new CompaniaDto();
		
		c.setIdCompania(compania.getIdCompania());
		c.setName(compania.getName());
		c.setCif(compania.getCif());
		
		List<JuegoDto> allJuegos = new ArrayList<>();
		for (Juego juego:compania.getJuegos()) {
			
			JuegoDto j = juegoConverter.juegoToDto(juego);
			allJuegos.add(j);
		}
		c.setJuegos(allJuegos);
		
		return c;
	}
	
	public Compania companiaToEnt(CompaniaDto companiaDto) {
		
		Compania c = new Compania();
		
		c.setIdCompania(companiaDto.getIdCompania());
		c.setName(companiaDto.getName());
		c.setCif(companiaDto.getCif());
		
		List<Juego> allJuegos = new ArrayList<>();
		for (JuegoDto juegoDto:companiaDto.getJuegos()) {
			
			Juego j = juegoConverter.juegoToEnt(juegoDto);
			allJuegos.add(j);
		}
		c.setJuegos(allJuegos);
		
		return c;
	}
}
