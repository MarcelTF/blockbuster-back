package com.blockbuster.project.exceptions;

import com.blockbuster.project.exceptions.generic.BadRequestException;

public class ClienteBadRequestException extends BadRequestException {

	private static final String DESCRIPTION = "Bad Request Exception (400)";
	
	public ClienteBadRequestException (String details) {
		
		super(DESCRIPTION + ", " + details);
	}
}
