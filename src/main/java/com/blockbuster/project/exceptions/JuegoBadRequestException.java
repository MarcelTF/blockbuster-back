package com.blockbuster.project.exceptions;

import com.blockbuster.project.exceptions.generic.BadRequestException;

public class JuegoBadRequestException extends BadRequestException {
	
	private static final String DESCRIPTION = "Bad Request Exception (400)";
	
	public JuegoBadRequestException (String details) {
		
		super(DESCRIPTION + ", " + details);
	}

}
