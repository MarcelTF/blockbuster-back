package com.blockbuster.project.exceptions.generic;

public class NotFoundException extends RuntimeException {
	
private static final String DESCRIPTION = "NotFoundException (404)";
	
	public NotFoundException(String details) {
		
		super(DESCRIPTION + ", " + details);
	}

}
