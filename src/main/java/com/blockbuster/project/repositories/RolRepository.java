package com.blockbuster.project.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.blockbuster.project.entities.Rol;
import com.blockbuster.project.enums.RolesEnum;

@Repository
public interface RolRepository extends JpaRepository<Rol, Long> {

	public Optional<List<Rol>> findByRol(RolesEnum rol);
}
