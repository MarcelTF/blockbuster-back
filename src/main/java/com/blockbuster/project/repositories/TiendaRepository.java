package com.blockbuster.project.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.blockbuster.project.entities.Tienda;

public interface TiendaRepository extends JpaRepository<Tienda, Long>{

	public List<Tienda> findByName(String name);
}
