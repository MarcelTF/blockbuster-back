package com.blockbuster.project.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.blockbuster.project.entities.Cliente;

@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Long>{

	public Cliente findByName(String name);
	
	public Cliente findByDni(String dni);
	
	public boolean existsByDni(String dni);

	public List<Cliente> findByUsername(String username);
	
}
