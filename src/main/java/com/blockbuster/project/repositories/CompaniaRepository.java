package com.blockbuster.project.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.blockbuster.project.entities.Compania;

@Repository
public interface CompaniaRepository extends JpaRepository<Compania, Long> {
	
	public Compania findByCif(String cif);

}
