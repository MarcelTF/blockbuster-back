package com.blockbuster.project.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.blockbuster.project.entities.Juego;

@Repository
public interface JuegoRepository extends JpaRepository<Juego, Long>{
	
	public List<Juego> findByTitle(String title);

}
