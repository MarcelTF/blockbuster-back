package com.blockbuster.project.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.blockbuster.project.entities.Stock;

@Repository
public interface StockRepository extends JpaRepository<Stock, Long>{
	
	public Optional<List<Stock>> findByReferencia(String referencia);

}
